package com.classpath.ekartoauth2client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkartOauth2ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkartOauth2ClientApplication.class, args);
	}

}
